package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Activity:
{
  "type": "object",
  "properties": {
    "uuid": {
      "description": "Unique identifier for the activity",
      "type": "string"
    }
  }
}
*/

public class Activity {

	@Size(max=1)
	private String uuid;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    uuid = null;
	}
	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}