package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Profile:
{
  "type": "object",
  "properties": {
    "first_name": {
      "description": "First name of the Uber user.",
      "type": "string"
    },
    "last_name": {
      "description": "Last name of the Uber user.",
      "type": "string"
    },
    "email": {
      "description": "Email address of the Uber user",
      "type": "string"
    },
    "picture": {
      "description": "Image URL of the Uber user.",
      "type": "string"
    },
    "promo_code": {
      "description": "Promo code of the Uber user.",
      "type": "string"
    }
  }
}
*/

public class Profile {

	@Size(max=1)
	private String first_name;

	@Size(max=1)
	private String last_name;

	@Size(max=1)
	private String email;

	@Size(max=1)
	private String picture;

	@Size(max=1)
	private String promo_code;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    first_name = null;
	    last_name = null;
	    email = null;
	    picture = null;
	    promo_code = null;
	}
	public String getFirst_name() {
		return first_name;
	}
	
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPicture() {
		return picture;
	}
	
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getPromo_code() {
		return promo_code;
	}
	
	public void setPromo_code(String promo_code) {
		this.promo_code = promo_code;
	}
}