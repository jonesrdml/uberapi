package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.UberAPI.UberAPIServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveHistoryByOffsetInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveHistoryByOffsetReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveHistoryByOffsetTests {

	@Test
	public void testOperationRetrieveHistoryByOffsetBasicMapping()  {
		UberAPIServiceDefaultImpl serviceDefaultImpl = new UberAPIServiceDefaultImpl();
		RetrieveHistoryByOffsetInputParametersDTO inputs = new RetrieveHistoryByOffsetInputParametersDTO();
		inputs.setOffset(0);
		inputs.setLimit(0);
		RetrieveHistoryByOffsetReturnDTO returnValue = serviceDefaultImpl.retrieveHistoryByOffset(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}